<?php


/**
 * Задача 8:
 * есть переменная $language. В ней может быть 2 варианта значений: 'en_GB' и ‘ru_RU‘';
 * нужно написать код, который в случае, если $language имеет значение 'en_GB' -  в переменную $months
 * запишет массив из 12 месяцев года на английском языке, если language === ‘ru_RU’ – то на русском;
 * нужно выполнить задание с помощью 3 способов: через 2 if, через switch-case, через многомерный массив и
 * без if’ов и switch-case.
 */

$language = 'en_GB';
$months = '';

$en_months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
$ru_months = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];

//решение через  switch case
switch ($language) {
    case 'en_GB':
        $months = $en_months;
        break;
    case 'ru_RU':
        $months = $ru_months;
        break;
};
//echo $months;

// решение через if
if('en_GB' === $language) {
    $months = $en_months;
}else if('ru_RU' === $language) {
    $months= $ru_months;
}
//echo $months;

// решение через через многомерный массив
$languageSelection = ['en_GB' => $en_months, 'ru_RU' => $ru_months];
$months = $languageSelection[$language];

//print_r($languageSelection[$language]);


/**
 * Задача 13 (на циклы):
 * нужно вывести в столбец числа от 1 до 75;
 * нужно использовать 2 разных цикла.
*/

//решение через  while
$number = 1;
while($number <= 75) {
//    echo '<pre>'.$number .PHP_EOL.'</pre>';
    $number++;
}

//решение через  for
for(; $number <= 75;$number++){
//    echo '<pre>'.$number .PHP_EOL.'</pre>';
}



/**
 * Задача 14 (на циклы):
 * есть массив с элементами 7, 5, 3, 1;
 * выведите квадрат каждого числа с новой строки.
 */

$numbers = [7, 5, 3, 1];

//Возведение чисел в квадрат Квадратный корень числа
foreach ($numbers as $value ) {
//    echo '<p>'.$value.'<sup>2</sup> = '.($value * $value).'</p>';
//    echo '<p> √ '.$value.' = '.sqrt($value).'</p>';
};



//advanced


/**
 * Задача 1:
 * есть число 5000;
 * нужно поделить его на 2 столько раз, пока результат деления не станет меньше 77;
 * выведите в конце число, которое получится;
 * посчитайте и выведите количество итераций (повторений цикла), которое понадобилось;
 * нужно решить задачу с помощью двух разных циклов.
 */

$numberDivision = 5000;

//решение через  for
for(; $numberDivision >= 77;) {
    $numberDivision /= 2;
}
//echo '<p>'.$numberDivision .'</p>';

//решение через  while
while($numberDivision >= 77){
    $numberDivision /= 2;
}
//echo '<p>'.$numberDivision.'</p>';

/**
 * Задача 2:
 * есть такой массив;
 * нужно перебрать его в цикле и записать в переменную $result список всех id (и ids) через точку с запятой (;);
 * вывести значение переменной $result на экран
 */

$arr = [
    'members' => [
        [
            'type' => 'user_to',
            'id' => 14,
            'last_view' => 1483023775,
            'username' => 'Petya',
        ],
        [
            'type' => 'user_from',
            'id' => 16,
            'ids' => [1, 5, 6, 10],
            'last_view' => 1483023775,
            'username' => 'Vova',
        ],
        'simple_user',
        [
            'type' => 'user_next',
            'id' => 26,
            'last_view' => 1483023772,
            'username' => 'Andrey',
        ],
        'advanced_user' => 'Sergei',
    ],
];

$result;
foreach($arr as $key => $value) {
    foreach($arr[$key] as $key_2 => $value_2) {
        foreach($arr[$key][$key_2] as $key_3 => $value_3) {
            if($key_3 === 'id' || $key_3 === 'ids') {
                if(is_array($value_3)){
                    $result .=$key_3.' = '. join(', ', $value_3).'; ';
                }else{
                    $result .= $key_3.' = '. $value_3.'; ';
                }
            }
        }
    }
}

//echo $result;

/**
 * Задача 3:
 * используя цикл for создайте массив из чисел от 1 до 77;
 * пример: [1, 2, 3, …, 77].
 */

$newArr = [];
for($i=0; $i<=77;$i++) {
    array_push($newArr, $i);
}
//print_r($newArr);